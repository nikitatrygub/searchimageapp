import React, { Component } from "react";
import { RefreshControl } from "react-native";
import { Content, Spinner, View } from "native-base";
import { fetchImages } from "../api/fetchImages";
import List from "../components/Home/List";
import Layout from "../components/Layout";
import ErrorView from "../components/Home/ErrorView";

class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.state = initialState = {
      photos: null,
      loading: true,
      error: false,
      isFetching: false,
      searchQuery: "",
      refreshing: false,
      isDataEmpty: false
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.getPhotos();
  }

  getPhotos = async () => {
    const { searchQuery } = this.state;
    try {
      const images = await fetchImages(1, searchQuery);
      this.setState({
        loading: false,
        photos: images,
        isDataEmpty: images.length < 10 ? true : false
      });
    } catch (e) {
      this.setState({
        loading: false,
        error: e.message
      });
    }
  };

  loadMorePhotos = async () => {
    const { photos, searchQuery } = this.state;
    this.page = this.page + 1;
    this.setState({
      isFetching: true
    });
    try {
      const images = await fetchImages(this.page, searchQuery);
      this.setState({
        loading: false,
        photos: [...photos, ...images],
        isFetching: false,
        isDataEmpty: images.length < 10 ? true : false
      });
    } catch (e) {
      this.setState({
        loading: false,
        error: e.message,
        isFetching: false
      });
    }
  };

  handleChangeSearch = value => {
    this.setState({
      searchQuery: value
    });
  };

  refresh = () => {
    this.setState(initialState, () => this.getPhotos());
  };

  render() {
    const {
      photos,
      loading,
      error,
      isFetching,
      searchQuery,
      refreshing,
      isDataEmpty
    } = this.state;
    return (
      <Layout
        handleChangeSearch={this.handleChangeSearch}
        searchQuery={searchQuery}
        searchImages={this.getPhotos}
      >
        <Content
          style={{
            backgroundColor: "#eeeeee"
          }}
          refreshControl={
            <RefreshControl onRefresh={this.refresh} refreshing={refreshing} />
          }
        >
          <View style={{ padding: 10 }}>
            {error && <ErrorView error={error} />}
            {loading && <Spinner color="black" />}
            <List
              photos={photos}
              loadMorePhotos={this.loadMorePhotos}
              isFetching={isFetching}
              isDataEmpty={isDataEmpty}
            />
          </View>
        </Content>
      </Layout>
    );
  }
}

export default MainScreen;
