import React from "react";
import { StyleSheet } from "react-native";
import { Container, Text } from "native-base";

const ErrorView = ({ error }) => (
  <Container
    style={{ padding: 10, alignItems: "center" }}
  >
    <Text style={styles.errorMessage}>{error}</Text>
    <Text>Please swipe down for refresh</Text>
  </Container>
);

const styles = StyleSheet.create({
  errorMessage: {
    marginTop: 20,
    marginBottom: 20
  },
  refreshButton: {
    width:'50%',
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#000000"
  }
});

export default ErrorView;
