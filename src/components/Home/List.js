import React, { Component } from "react";
import { View, Text } from "native-base";
import { FlatList, Image, StyleSheet } from "react-native";
import LoadMoreButton from "./LoadMoreButton";

class List extends Component {
  renderItem = item => {
    return (
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={{ uri: item.urls.small }} />
      </View>
    );
  };
  renderStub = () => {
    return (
      <View style={styles.empty}>
        <Text style={styles.emptyText}>Oops , nothing was found</Text>
      </View>
    );
  };
  render() {
    const { photos, loadMorePhotos, isFetching, isDataEmpty } = this.props;
    return (
      <View>
        {photos && photos.length === 0 && this.renderStub(photos)}
        {photos && (
          <View>
            <FlatList
              numColumns={2}
              data={photos}
              renderItem={({ item }) => this.renderItem(item)}
              keyExtractor={(item, index) => index.toString()}
            />
            {!isDataEmpty && (
              <LoadMoreButton
                loadMorePhotos={loadMorePhotos}
                loadMore={isFetching}
              />
            )}
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    flexDirection: "column",
    margin: 4
  },
  image: {
    width: "100%",
    height: 200,
    borderColor: "#ffffff",
    borderWidth: 2
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10
  },
  emptyText: {
    fontSize: 18
  }
});

export default List;
