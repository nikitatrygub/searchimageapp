import React from "react";
import { StyleSheet } from "react-native";
import { Button, Text, Body, Spinner } from "native-base";

class LoadMoreButton extends React.Component {
  renderLodMoreButton = () => {
    const { isFetching , loadMorePhotos } = this.props;
    return !isFetching && (
      <Button
        style={styles.loadMoreButton}
        disabled={isFetching}
        onPress={loadMorePhotos}
      >
        <Text>Load more</Text>
      </Button>
    );
  };

  render() {
    const { isFetching } = this.props;
    return (
      <Body style={{ flexDirection: "column" }}>
        {isFetching && <Spinner color="black" />}
        {this.renderLodMoreButton()}
      </Body>
    );
  }
}

const styles = StyleSheet.create({
  loadMoreButton: {
    alignItems: "center",
    marginTop: 40,
    marginBottom: 40,
    justifyContent: "center",
    backgroundColor: "#000000"
  }
});

export default LoadMoreButton;
